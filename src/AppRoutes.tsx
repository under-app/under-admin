import { BrowserRouter, Navigate, Outlet, Route, Routes } from "react-router-dom";

import Logout from "./pages/logout/Logout";
import UsersPage from "./pages/users/Users";
import MainLayout from "./layouts/MainLayout";
import LoginPage from "./pages/login/LoginPage";
import CustomersPage from "./pages/customers/Customers";
import DashboardPage from "./pages/dashboard/DashboardPage";

const RouteGuard = ({ isPrivate = false, fallbackRoute = "" }: { isPrivate?: boolean; fallbackRoute?: string }) => {
    const isAuthenticated = localStorage.getItem("access_token") !== null;

    if ((isPrivate && !isAuthenticated) || (!isPrivate && isAuthenticated)) {
        return <Navigate to={`/${fallbackRoute}`} />;
    }

    return <Outlet />;
};

export enum routes {
    root = "/",
    login = "/login",
    logout = "/logout",
    users = "/users",
    customers = "/customers",
    settings = "/settings",
    register = "/register",
}

const AppRoutes = () => (
    <BrowserRouter>
        <Routes>
            <Route element={<RouteGuard />}>
                <Route element={<MainLayout mode="login" />}>
                    <Route path={routes.login} element={<LoginPage />} />
                </Route>
            </Route>

            <Route element={<RouteGuard isPrivate fallbackRoute="login" />}>
                <Route element={<MainLayout />}>
                    <Route path={routes.root} element={<DashboardPage />} />
                    <Route path={routes.users} element={<UsersPage />} />
                    <Route path={routes.customers} element={<CustomersPage />} />
                    <Route path={routes.logout} element={<Logout />} />
                </Route>
            </Route>
        </Routes>
    </BrowserRouter>
);

export default AppRoutes;
