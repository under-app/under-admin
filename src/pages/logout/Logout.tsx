import { useEffect } from "react";

import { Outlet, useNavigate } from "react-router-dom";

const Logout = () => {
    const navigate = useNavigate();

    useEffect(() => {
        // TODO: clear store
        localStorage.removeItem("access_token");
        navigate("/login");
    }, [navigate]);

    return <Outlet />;
};

export default Logout;
