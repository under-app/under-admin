import { Suspense } from "react";

import CssBaseline from "@mui/material/CssBaseline";
import { createTheme, ThemeProvider } from "@mui/material/styles";

import AppRoutes from "./AppRoutes";

const theme = createTheme();

const App = () => {
    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />

            <Suspense fallback="loading">
                <AppRoutes />
            </Suspense>
        </ThemeProvider>
    );
};

export default App;
